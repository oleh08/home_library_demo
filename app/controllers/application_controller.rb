class ApplicationController < ActionController::Base
  include DeviseTokenAuth::Concerns::SetUserByToken
  protect_from_forgery with: :exception

  protect_from_forgery prepend: true
  # skip_before_action :verify_authenticity_token
end
